
/**
 * Represents a RollingBallsGame player.
 * @author Ivan J. Miranda
 *
 */
public class Player
{
	private String userName;
	private String password;
	private int highScore = 0;
	private String dateOfHighScore = "-";
	
	/**
	 * Constructs an Player object given the parameters specified. This constructor should be strictly used to read from files 
	 * existing data, unless you really want to input specific dates and high scores at the creation of the player.
	 * @param userName the player's username.
	 * @param password the player's password.
	 * @param highScore the player's all time high score.
	 * @param dateOfHighScore the date of the player's all time high score.
	 */
	public Player(String userName, String password, int highScore, String dateOfHighScore)
	{
		this.userName = userName;
		this.password = password;
		this.highScore = highScore;
		this.dateOfHighScore = dateOfHighScore;
		
	}
	
	/**
	 * Constructs an Player object given the parameters specified. This is the constructor that should be used to create player's from blank.
	 * When it sets a new high score, the high score and its date should be modified by the setters as per the specifications.
	 * @param userName the player's username.
	 * @param password the player's password.
	 */
	public Player(String userName, String password)
	{
		this.userName = userName;
		this.password = password;
	}
		
	/**
	 * Gets this player's username.
	 * @return this player's username.
	 */
	public String getUserName()
	{
		return userName;
	}
	
	/**
	 * Gets this player's password.
	 * @return this player's password.
	 */
	public String getPassword()
	{
		return password;
	}
	
	/**
	 * Gets this player's high score.
	 * @return this player's high score.
	 */
	public int getHighScore()
	{
		return highScore;
	}
	
	/**
	 * Sets this player high score to a new high score.
	 * @param highScore the new high score of this player.
	 */
	public void setHighScore(int highScore)
	{
		this.highScore = highScore;
	}
	
	/**
	 * Gets the String representation of the date of this player's high score as per the format used to create this date.
	 * @return the date of this player's high score.
	 */
	public String getDateOfHighScore()
	{
		return dateOfHighScore;
	}
	
	/**
	 * Sets the date of this player's high score as a String representation of it already formatted as wanted.
	 * @param dateOfHighScore the date of this player's high score.
	 */
	public void setDateOfHighScore(String dateOfHighScore)
	{
		this.dateOfHighScore = dateOfHighScore;
	}
	
	/**
	 * Convert's this player to its String representation, giving username, password, high score, and date of high score separated by a space.
	 * @return the String representation of this player.
	 */
	public String toString()
	{
		return userName + " " + password + " " + highScore + " " + dateOfHighScore;
	}
	
} //End Player class.
