import java.util.Comparator;

/**
 * A comparator object that allows you to compare Player objects by their high score. Highest scores come first.
 * @author Ivan J. Miranda
 *
 */
public class PlayerListScoreComparator implements Comparator<Player>
{
	@Override
	/**
	 * Compares two Player objects by their high scores. Highest first.
	 * @return an negative integer if p1's high score is greater than p2's high score, 0 if p1's high score is equal to p2's high score,
	 * or a positive integer if p1's high score is less than p2's high score, as by the natural order of numbers.
	 */
	public int compare(Player p1, Player p2) 
	{
		return p2.getHighScore() - p1.getHighScore(); //RETURNS AS IT WOULD FOR NATURAL NUMBERS ORDERING, BUT HIGHEST FIRST.
	}
}
