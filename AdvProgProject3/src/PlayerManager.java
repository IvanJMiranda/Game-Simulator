import java.io.*;
import java.util.*;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A static class containing methods that manage the list of current players and interacts with the user via the programs GUI.
 * @author Ivan J. Miranda
 *
 */
public class PlayerManager
{
	static boolean noInitialPlayers; //Flag that indicates that no players exist.
	public static Player currentPlayer1 = null; //Selected player 1.
	public static Player currentPlayer2 = null; //Selected player 2.
	public static final int PLAYER_ONE = 1; //For readability.
	public static final int PLAYER_TWO = 2; //For readability.
	
	/**
	 * This method creates a new dialog that lets the user create a new player given the ArrayList of Player objects where it will be stored
	 * and the file where it will be written immediately if the creation of the player is successful.
	 * @param playerList the ArrayList of Player objects in which the player will be stored.
	 * @param playerListFile the File where the current registered players data will be written.
	 * @return true if and only if the player was created and written to file successfully. Else returns false, for instance, if the user
	 * clicks the CANCEL button.
	 */
	public static boolean createPlayerDialog(ArrayList<Player> playerList, File playerListFile)
	{
		final int MAX_CHARACTERS = 15; //Max characters for either username or password.
		boolean validUserName = false; //Flag that indicates a valid username.
		boolean validPassword = false; //Flag that indicates a valid password.
		String inputUserName = "";
		String inputPassword = "";
		
		//Validates username and password.
		do
		{
			//USERNAME VALIDATION
			if(!validUserName)
			{
				inputUserName = JOptionPane.showInputDialog("Enter username: (Max " + MAX_CHARACTERS + " characters and no spaces)");
				
				if(!(inputUserName == null)) //Dialog returns null if CANCEL option is selected.
				{
					if(inputUserName.length() > MAX_CHARACTERS)
					{
						JOptionPane.showMessageDialog(null, "Username is longer than " + MAX_CHARACTERS + " characters!", "Error!", JOptionPane.ERROR_MESSAGE);
					}
					else //Username contains MAX_CHARACTERS characters or less.
					{
						if(inputUserName.isEmpty() || inputUserName.contains(" "))
						{
							JOptionPane.showMessageDialog(null, "Your username cannot contain any spaces!", "Error!", JOptionPane.ERROR_MESSAGE);
						}
						else //Username does not contain spaces nor is empty.
						{
							if(ArrayListPlayerSearch.searchByUserName(playerList, inputUserName) != null) //Player already exists
							{
								JOptionPane.showMessageDialog(null, "The player \"" + inputUserName + "\" already exists!", "Error!", JOptionPane.ERROR_MESSAGE);
							}
							else //Player doesn't exist.
							{
								validUserName = true;
							}
							
						}
						
					}	
					
				} //Username validated.
			
				else
				{
					return false; //User selected CANCEL option.
				} 
				
			} //End valid userName check.
			
			//PASSWORD VALIDATION.
			if(validUserName && !validPassword)
			{
				inputPassword = JOptionPane.showInputDialog("Enter password: (Max " + MAX_CHARACTERS + " characters)");
				
				if(!(inputPassword == null)) //User didn't select cancel option.
				{
					if(inputPassword.length() > MAX_CHARACTERS)
					{
						JOptionPane.showMessageDialog(null, "Password is longer than " + MAX_CHARACTERS + " characters!", "Error!", JOptionPane.ERROR_MESSAGE);
					}
					else //Password contains MAX_CHARACTERS characters or less.
					{
						if(inputPassword.isEmpty() || inputPassword.contains(" ")) //Password is empty or contain spaces
						{
							JOptionPane.showMessageDialog(null, "Your password cannot contain any spaces!", "Error!", JOptionPane.ERROR_MESSAGE);
						}
						else
						{
							validPassword = true; //PASSWORD VALIDATED.
						}
					}
				}
				else
				{
					return false; //User selected CANCEL option.
				}
				
			} //End valid password check.
		
		}while(!validUserName || !validPassword); //FINISHES WHEN BOTH USERNAME AND PASSWORD AR VALID OR CANCEL OPTION IS SELECTED, AS IT RETURNS NULL
												  //IN THAT CASE.
		
		//PLAYER VALIDATION PASSED. PROCEEDING TO STORE IN ARRAYLIST OF PLAYER OBJECTS AND WRITE TO FILE.
		playerList.add(new Player(inputUserName, inputPassword));
		try {
			writeToFile(playerList, playerListFile);
		} catch (IOException e) {
			playerList.remove(playerList.size() - 1); //If any exception occurs, the player is removed and the method returns false.
			JOptionPane.showMessageDialog(null, "Sorry. An unexpected error ocurred. "
					+ "Please try creating a player again.", "Error", JOptionPane.ERROR_MESSAGE);
			return false; //An unexpected error occurred. Player NOT saved.
		}
		
		JOptionPane.showMessageDialog(null, "Player created succesfully!", "Congrats!", JOptionPane.INFORMATION_MESSAGE);
		return true; //PLAYER WAS A VALID PLAYER AND WAS CREATED AND SAVED TO FILE SUCCESFULLY.
		
	} //End createPlayerDialog method.
	
	/**
	 * This method creates a new dialog in which the user can select from existing players through the GUI, if any. Else,
	 * it proceeds first to the player creation dialog.
	 * @param playerList the ArrayList of Player objects from which the player will be selected.
	 * @param playerListFile the file where the player's are stored in case there are not existing players and new players need to be created.
	 * @param playerNumber the number of the player which is making the selection, Player 1 or Player 2. Use this class constants
	 * PlayerManager.PLAYER_ONE or PlayerManager.PLAYER_TWO to guarantee the proper function of the method.
	 */
	public static void selectPlayerDialog(ArrayList<Player> playerList, File playerListFile, int playerNumber)
	{
		boolean operationCancelled = false; //Flag for clicking the CANCEL button.
		noInitialPlayers = false; 
		
		//CHECKS IF THERE ARE AT LEAST TWO PLAYERS IN FILE.
		if(playerList.size() < 2)
		{
			if(playerList.size() == 0) //NO PLAYERS
			{
				JOptionPane.showMessageDialog(null, "There are no players registered. "
						+ "The game will now take you twice to the registration dialog.", "Error", JOptionPane.ERROR_MESSAGE);
				operationCancelled = !createPlayerDialog(playerList, playerListFile); //Creates a new player dialog.
				if(!operationCancelled)
				{
					JOptionPane.showMessageDialog(null, "Create now the second player.", "Almost there!", JOptionPane.INFORMATION_MESSAGE);
					operationCancelled = !createPlayerDialog(playerList, playerListFile); //If CANCEL button was not pressed previously, creates
																						  //player 2 dialog.
					noInitialPlayers = true;
				}
			}
			else //AT LEAST ONE PLAYER AND IT IS ALREADY SELECTED FOR THE OPPONENT OF this PLAYER.
			{
				if((playerNumber == 1 && currentPlayer2 != null) || (playerNumber == 2 && currentPlayer1 != null))
				{
				JOptionPane.showMessageDialog(null, "There's only one player registered. "
					+ "The game will now take you once to the First Time Players dialog.", "Error", JOptionPane.ERROR_MESSAGE);
				operationCancelled = !createPlayerDialog(playerList, playerListFile); //If the player that wants to select a player is not the one
																					  //that already has selected himself, then create a new player.
				}
			}
		} //End not enough players check.
		
		//THERE WERE ENOUGH PLAYERS OR THERE WEREN'T BUT WERE CREATED. (CANCEL BUTTON WAS NOT PRESSED)
		if(!operationCancelled)
		{
			playerList.sort(new PlayerListNameComparator()); //Sorts the player list by name.
			int playerListWidth = (int)(TheRollingBallsGame.gameWindowWidth/3.0f);
			int playerListHeight = (int)(TheRollingBallsGame.gameWindowHeight/3.5f);
			
			String[] playerName = new String[playerList.size()]; //Array of names that will be shown in JList.
			for(int i = 0; i < playerList.size(); i++)
			{
				playerName[i] = playerList.get(i).getUserName();
			}
			//PLAYER NAMES WERE SORTED AND EXTRACTED TO THE JLIST
			
			JList<String> graphicalPlayerList = new JList<>(playerName);
			graphicalPlayerList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			graphicalPlayerList.setFixedCellWidth(playerListWidth);
			JScrollPane graphicalPlayerListScroll = new JScrollPane(graphicalPlayerList);
			
			//PREPARES FOR THE CREATION OF A NEW SELECTION FRAME WHERE YOU CLICK THE NAME AND CLICK OK TO SELECT A PLAYER.
			JFrame playerSelectionWindow = new JFrame("Select player " + playerNumber);
			
			JButton okButton = new JButton("OK");
			okButton.addActionListener(new ActionListener()
			{	 //OK BUTTON ACTION IF CLICKED.
				 public void actionPerformed(ActionEvent e)
		            {
		                String playerSelected = graphicalPlayerList.getSelectedValue();
		                if(playerSelected == null) //CHECKS IF A NAME WAS SELECTED
		                {
		        			JOptionPane.showMessageDialog(null, "You didn't select any player!", "Error!", JOptionPane.ERROR_MESSAGE);
		                }
		                
		                else //A NAME WAS SELECTED AND CHECKS FOR WHICH PLAYER THE NAME WAS SELECTED, CHECKING IF IT IS ALREADY SELECTED.
		                {
		                	if(playerNumber == PLAYER_ONE) 
		                	{
		                		if(currentPlayer2 != null && playerSelected.equals(currentPlayer2.getUserName()))
		                		{
		                			JOptionPane.showMessageDialog(null, "This player is already selected!", "Error!", JOptionPane.ERROR_MESSAGE);
		                		}
		                		else
		                		{	//ASKS FOR PASSWORD TO PROCEED.
		                			String inputPassword;
		                			boolean correctPassword;
		                			
		                			do{
		                				inputPassword = JOptionPane.showInputDialog("Enter password: ");
		                				correctPassword = false;
		                				if(ArrayListPlayerSearch.searchByUserName(playerList, playerSelected).getPassword().equals(inputPassword))
		                				{	//ENTERED CORRECT PASSWORD.
		                					correctPassword = true;
		                					currentPlayer1 = ArrayListPlayerSearch.searchByUserName(playerList, playerSelected); 
		                					playerSelectionWindow.dispose();
		                					if(noInitialPlayers)
		                						selectPlayerDialog(playerList, playerListFile, PlayerManager.PLAYER_TWO);
		                				}
		                				else //ENTERED INCORRECT PASSWORD. NO LIMIT OF ATTEMPTS.
		                				{
		                					if (inputPassword != null)
		                						JOptionPane.showMessageDialog(null, "Your password is incorrect! Please try again.", "Error!", JOptionPane.ERROR_MESSAGE);
		                				}
		                				
		                			} while(inputPassword != null && !correctPassword);	//ENDS WHEN CANCEL BUTTON IS PRESSED OR CORRECT PASSW ENTERED.
		                		}
		                	}
		                		
		                	if(playerNumber == PLAYER_TWO)
		                	{
		                		if(currentPlayer1 != null && playerSelected.equals(currentPlayer1.getUserName()))
		                		{
		                			JOptionPane.showMessageDialog(null, "This player is already selected!", "Error!", JOptionPane.ERROR_MESSAGE);
		                		}
		                		else
		                		{	//ASKS FOR PASSWORD TO PROCEED.
		                			String inputPassword;
		                			boolean correctPassword;
		                			
		                			do{ //CORRECT PASSWORD.
		                				inputPassword = JOptionPane.showInputDialog("Enter password: ");
		                				correctPassword = false;
		                				if(ArrayListPlayerSearch.searchByUserName(playerList, playerSelected).getPassword().equals(inputPassword))
		                				{
		                					correctPassword = true;
		                					currentPlayer2 = ArrayListPlayerSearch.searchByUserName(playerList, playerSelected);  
		                					playerSelectionWindow.dispose();
		                					if(noInitialPlayers)
		                						selectPlayerDialog(playerList, playerListFile, PlayerManager.PLAYER_ONE);
		                				}
		                				else //INCORRECT PASSWORD. NO LIMIT OF ATTEMPTS.
		                				{
		                					if (inputPassword != null)
		                						JOptionPane.showMessageDialog(null, "Your password is incorrect! Please try again.", "Error!", JOptionPane.ERROR_MESSAGE);
		                				}
		                				
		                			} while(inputPassword != null && !correctPassword); //ENDS WHEN CANCEL BUTTON IS PRESSED OR CORRECT PASSW ENTERED.
		                		}
		                	}
		                }
		                
		                TheRollingBallsGame.gameWindow.repaint(); //UPDATES CURRENT PLAYER SELECTIONS IN TITLE SCREEN.
		                
		            } //End actionPerformed method.
				 
			}); //End ActionListener.
			
			JButton cancelButton = new JButton("CANCEL");
			cancelButton.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					playerSelectionWindow.dispose(); //CLOSES THE WINDOW WITHOUT APPLYING ANY CHANGES.
				}
			});
			
			//ADD CONTENTS TO A JPANEL, AND IT TO JFRAME.
			JPanel selectionWindow = new JPanel();
			selectionWindow.add(graphicalPlayerListScroll, Container.CENTER_ALIGNMENT);
			selectionWindow.add(okButton, Container.BOTTOM_ALIGNMENT);
			selectionWindow.add(cancelButton, Container.BOTTOM_ALIGNMENT);
			selectionWindow.setBackground(Color.WHITE);
			
			playerSelectionWindow.add(selectionWindow);
			playerSelectionWindow.setSize(playerListWidth + 25, playerListHeight);
			playerSelectionWindow.setLocation((int)(7f*TheRollingBallsGame.gameWindowWidth/10), (int)(4f*TheRollingBallsGame.gameWindowHeight/10));
			playerSelectionWindow.setResizable(false);
			playerSelectionWindow.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			playerSelectionWindow.setVisible(true);	 //FRAME IS CREATED, READY AND VISIBLE.		
		}
		
	} //End selectPlayerDialog method.
	
	/**
	 * Show players stored in an ArrayList in a visible list, ordered by the given comparator.
	 * @param playerList the ArrayList of Player objects.
	 * @param cmp the comparator that establishes the order relation for the players.
	 */
	public static void showPlayersBy(ArrayList<Player> playerList, Comparator<Player> cmp)
	{
		playerList.sort(cmp); //Sort as per the cmp.
		int playerListWidth = (int)(TheRollingBallsGame.gameWindowWidth/3.0f);
		int playerListHeight = (int)(TheRollingBallsGame.gameWindowHeight/3.5f);
		
		String[] player = new String[playerList.size() + 2]; //First two are for heading purposes.
		player[0] = String.format("%-20s%-20s%-20s", "Username", "High Score", "Date(YYYY/MM/DD-Time)"); //Header row.
		player[1] = " "; //blank line (divider).
		for(int i = 0; i < playerList.size(); i++)
		{
			Player temp = playerList.get(i);
			player[i + 2] = String.format("%-22s%-22s%-22s", temp.getUserName(), temp.getHighScore(), temp.getDateOfHighScore());
		}
		//SCORE ENTRIES WERE SORTED AND EXTRACTED TO THE JLIST
		
		JList<String> graphicalPlayerList = new JList<>(player);
		graphicalPlayerList.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		graphicalPlayerList.setFixedCellWidth(playerListWidth);
		JScrollPane graphicalPlayerListScroll = new JScrollPane(graphicalPlayerList);
		
		//PREPARES FOR THE CREATION OF A NEW FRAME THAT SHOWS THE ORDERED LIST.
		JFrame playerListWindow = new JFrame("Registered Players");
		
		JButton okButton = new JButton("OK");
		okButton.addActionListener(new ActionListener()
		{
			 public void actionPerformed(ActionEvent e)
	            {
	                playerListWindow.dispose(); //Closes window.
	            } 
		}); 
		
		//ADDS COMPONENTS TO JPANEL, AND IT TO JFRAME.
		JPanel selectionWindow = new JPanel();
		selectionWindow.add(graphicalPlayerListScroll, Container.CENTER_ALIGNMENT);
		selectionWindow.add(okButton, Container.BOTTOM_ALIGNMENT);
		selectionWindow.setBackground(Color.WHITE);
		
		playerListWindow.add(selectionWindow);
		playerListWindow.setSize(playerListWidth + 25, playerListHeight);
		playerListWindow.setLocation((int)(7f*TheRollingBallsGame.gameWindowWidth/15), (int)(4f*TheRollingBallsGame.gameWindowHeight/10));
		playerListWindow.setResizable(false);
		playerListWindow.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		playerListWindow.setVisible(true);	 //FRAME IS CREATED, READY AND VISIBLE.	
		
	} //End showPlayersBy method.
	
	/**
	 * Read players from file assuming that each are stored as a single line that separates each of their instance variables by a space,
	 * and with NO new lines at the end, and stores them in an ArrayList of Player objects, and returns the list.
	 * @param playerListFile the File from where the players will be read.
	 * @return an ArrayList of Player objects that contain the players stored in file. Returns null if the file didn't exist and could
	 * not be created.
	 */
	public static ArrayList<Player> readPlayersFromFile(File playerListFile) //throws FileNotFoundException
	{
		ArrayList<Player> playerList = new ArrayList<Player>();
		
		//TODO consider doing a FileUtils generic method that receives a reader operator and a list.
		try {
			Scanner playerListIn = new Scanner(playerListFile);
			
			//TODO consider doing a "reader" interface and a playerReader that receives the data file and a player list.
			while(playerListIn.hasNextLine())
			{
				playerList.add(new Player(playerListIn.next(), playerListIn.next(), Integer.parseInt(playerListIn.next()), playerListIn.next()));
			}
			//TODO consider doing a "reader" interface and a playerReader that receives the data file and a player list.
			
			playerListIn.close();
		} catch (FileNotFoundException e) { //FILE WAS NOT FOUND. PROCEEDING TO CREATE IT.
			try {
				new FileWriter(playerListFile).close(); //TRYING TO CREATE FILE.
			} catch (IOException e1) {
				return null; //FILE COULD NOT BE CREATED. //TODO CONSIDER RETURNING THE EXCEPTION (LIKE WRITETOFILE METHOD AND DOING THE SAME WITH A TRY/CATCH.
			}
			playerList = readPlayersFromFile(playerListFile); //FILE WAS SUCCEFULY CREATED. WE CALL THE METHOD AGAIN TO RECEIVE THE
															  //PLAYER LIST, EVEN IF IT'S EMPTY.
		}
		//TODO consider doing a FileUtils generic method that receives a reader operator and a list.
		
		return playerList; //PLAYERS WERE READ SUCCEFULLY, IF ANY. FILE WAS CREATED IF IT DIDN'T EXIST. (IT DOESN'T EXIST IF IS IS
						   //THE FIRST TIME THE GAME IS EXECUTED BY ANY USER.
	} //End readFromFile method.
	
	/**
	 * Writes to file the given ArrayList of Player objects by their String representation. (toString() method)
	 * @param playerList the ArrayList of Player objects to be written to file.
	 * @param playerListFile the file where the ArrayList of Player objects will be written.
	 * @throws IOException if for any reason an error occurs in the writing process.
	 */
	public static void writeToFile(ArrayList<Player> playerList, File playerListFile) throws IOException
	{
		FileWriter playerListOut = new FileWriter(playerListFile, false); //PLAYER'S DATABASE OR SAVE FILE.
		for(int i = 0; i < playerList.size(); i++)
		{
			if(i != playerList.size() - 1) //Writes new line at the end if it is not the last player on the list.
			{
				playerListOut.write(playerList.get(i).toString());
				playerListOut.write(String.format("%n"));
			}
			else
				playerListOut.write(playerList.get(i).toString());
		}
		playerListOut.close(); //PLAYER SUCCESFULLY SAVED.
		
	} //End writeToFileMethod.
	
	/**
	 * Clears current player selections.
	 */
	public static void clearPlayerSelections() //TODO JUST CLEARS THE PLAYERS SELECTIONS. IT DOES NOT DELETE PLAYERS. CONSIDER ADDING THAT OPTION ANYWAY.
	{
		currentPlayer1 = null;
		currentPlayer2 = null;
		TheRollingBallsGame.gameWindow.repaint(); //Updates title screen.
	}
	
} //End PlayerManager class.
