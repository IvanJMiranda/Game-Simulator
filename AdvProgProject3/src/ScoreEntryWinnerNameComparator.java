import java.util.Comparator;

/**
 * A comparator object that allows you to compare ScoreEntry objects by their winner's name. It follows the natural lexicographical order of strings.
 * @author Ivan J. Miranda
 *
 */
public class ScoreEntryWinnerNameComparator implements Comparator<ScoreEntry>
{
	@Override
	/**
	 * Compares two ScoreEntry objects by their winner's name.
	 * @return a negative integer if sc1's winner's name precedes p2's winner's name, 0 if sc1's winner's name is equal to sc2's winner's name,
	 * or a positive integer if sc1's winner's name goes after sc2's winner's name, lexicographically.
	 */
	public int compare(ScoreEntry sc1, ScoreEntry sc2) 
	{
		return sc1.getWinnerName().compareToIgnoreCase(sc2.getWinnerName()); //COMPARES IGNORING CASE AND RETURNS AS IT WOULD FOR NATURAL LEXICOGRAPHICAL ORDER.
	}	
}
