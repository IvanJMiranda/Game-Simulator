import java.util.Comparator;

/**
 * A comparator object that allows you to compare ScoreEntry objects by their winner's score. Highest scores come first.
 * @author Ivan J. Miranda
 *
 */
public class ScoreEntryWinnerScoreComparator implements Comparator<ScoreEntry>
{
	@Override
	/**
	 * Compares two ScoreEntry objects by their winner's scores. Highest first.
	 * @return an negative integer if sc1's winner's score is greater than sc2's winner's score, 0 if sc1's winner's 
	 * score is equal to sc2's winner's score, or a positive integer if sc1's winner's score is less than sc2's winner's 
	 * score, as by the natural order of numbers.
	 */
	public int compare(ScoreEntry sc1, ScoreEntry sc2) 
	{
		return sc2.getWinnerScore() - sc1.getWinnerScore();
	}
}
